const request = require('supertest');
const app = require('../index'); 

describe('POST /tasks', () => {
    it('should create a new task and return it', async () => {
        const res = await request(app)
            .post('/tasks')
            .send({
                title: "Test Task"
            })
            .expect(201)
            .expect('Content-Type', /json/);
        expect(res.body).toHaveProperty('id');
        expect(res.body.title).toEqual('Test Task');
    });
});
