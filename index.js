const express = require('express');
const app = express();
const cors = require('cors');

app.use(express.json()); 
app.use(cors());  // Enable CORS for all routes

let tasks = []; // In-memory store for tasks

// Define API routes 
app.get('/tasks', (req, res) => {
    res.status(200).json(tasks);
});

app.post('/tasks', (req, res) => {
    const { title } = req.body;
    const newTask = { id: tasks.length + 1, title };
    tasks.push(newTask);
    res.status(201).json(newTask);
});

app.put('/tasks/:id', (req, res) => {
    const task = tasks.find(t => t.id === parseInt(req.params.id));
    if (!task) {
        return res.status(404).send('Task not found.');
    }
    task.title = req.body.title;
    res.status(200).json(task);
});

app.delete('/tasks/:id', (req, res) => {
    tasks = tasks.filter(t => t.id !== parseInt(req.params.id));
    res.status(204).send();
});

// Only start the server if the file was not required by another module
if (!module.parent) {
    const PORT = process.env.PORT || 3000;
    app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
};

app.use((req, res, next) => {
    console.log(`${req.method} ${req.path}`);
    next();
});


module.exports = app; // Export Express application instance for testing
