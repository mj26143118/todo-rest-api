const apiBaseURL = 'http://localhost:3000/tasks';

document.getElementById('addForm').onsubmit = function(event) {
    event.preventDefault();
    const taskTitle = document.getElementById('newTask').value;
    fetch(apiBaseURL, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ title: taskTitle })
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);
        document.getElementById('newTask').value = ''; // Clear input after adding
        getTasks(); // Reload tasks
    })
    .catch(error => console.error('Error adding task:', error));
};

document.getElementById('updateForm').onsubmit = function(event) {
    event.preventDefault();
    const taskId = document.getElementById('updateId').value;
    const taskTitle = document.getElementById('updateTitle').value;
    fetch(`${apiBaseURL}/${taskId}`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ title: taskTitle })
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);
        getTasks(); // Reload tasks
    })
    .catch(error => console.error('Error updating task:', error));
};

function getTasks() {
    fetch(apiBaseURL)
    .then(response => response.json())
    .then(tasks => {
        const taskList = document.getElementById('taskList');
        taskList.innerHTML = ''; // Clear existing tasks
        tasks.forEach(task => {
            const listItem = document.createElement('li');
            listItem.textContent = `ID ${task.id}: ${task.title} `;
            const deleteBtn = document.createElement('button');
            deleteBtn.textContent = 'Delete';
            deleteBtn.onclick = function() {
                deleteTask(task.id);
            };
            listItem.appendChild(deleteBtn);
            taskList.appendChild(listItem);
        });
    })
    .catch(error => console.error('Error loading tasks:', error));
}

function deleteTask(taskId) {
    fetch(`${apiBaseURL}/${taskId}`, {
        method: 'DELETE'
    })
    .then(() => {
        getTasks(); // Reload tasks
    })
    .catch(error => console.error('Error deleting task:', error));
}
